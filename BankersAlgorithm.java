
public class BankersAlgorithm {
	public static void main(String[] args) {
		int MAX_RESOURCES = 128;
		int resources = Integer.parseInt(args[1]);
		int threads = Integer.parseInt(args[2]);

		//Generate resources
		int[] r = new int[resources];
		int[] max_demand = new int[resources];
		for (int i = 0; i < resources; i++) {
			r[i] = (int) (Math.random()*MAX_RESOURCES);
			max_demand[i] = (int) (Math.random() * r[i]) + 1;
		}

		int[] available = new int[resources];
		//TODO: need to fill available array

		//Generate threads
		Thread[] t = new Thread[threads];
		for (int i = 0; i < threads; i++) {
			t[i] = new Thread(new BAThread(available));
		}
	}
}
